"use strict";

//----------COMMENT ALBUM------------//


function getMessage(){

    const requestAjax = new XMLHttpRequest();

    requestAjax.open("GET", "comment");

    requestAjax.responseType = 'json';

    requestAjax.send();

    requestAjax.onload = function(){

        const result= requestAjax.response;

        const html = result.map(function(comments){

            return `
            <div class="text-light p-2 m-1 ">
            <p class="text-uppercase firstnamecom">${comments.author}</p>
            <p class ="text-capitalize">${comments.content}</p>
            </div>
            `
        }).join('');

        var div_comment = document.querySelector('#display_comments');

        div_comment.innerHTML = html;

    }

}

getMessage();






function postMessage(event){

    event.preventDefault();

    const author = document.querySelector('#author');

    const content = document.querySelector('#content');

    const data = new FormData();

    data.append('author', author.value);

    data.append('content', content.value);

    const requestAjax = new XMLHttpRequest();

    requestAjax.open("POST", "comment?task=write");

    requestAjax.onload = function(){

        var  reponse= requestAjax.response;

        var res = reponse.trim();

        if(res == ""){
            
           getMessage();
           author.value = "";
           content.value = "";
           
       }else{
        
         alert('You must type author and content fields.');
     }

 }

 requestAjax.send(data);

}

document.querySelector('#comment_form').addEventListener('submit', postMessage);


//_____________RATING STARS__________//

$( document ).ready(function() {

    rate();
    displayRate();

});


function rate(){

    $(".tde").click(function(){

        var nbr = $(this).prop('id').substring(4);
        $.ajax({
            url: "rating",
            method: 'GET',
            data: { param1: nbr},
        }).done(function(result) {

            var res = JSON.parse(result);

            if(res['status'] == 'error'){
                alert("You have already rate");
            }
            
            displayRate();

        });

    });

};

function displayRate(){

    $.ajax({
        url: "get_average",
        method: 'GET',
        
    }).done(function(res) {

        var result = JSON.parse(res);

        var avg = Math.round(result['rate']);

        $('#tde_1').css('color', "orange");

        $("#avaragedb").text(avg);

        switch(avg){
            case -1 :
            $("#tde_1").css('color', "orange" );
            $("#tde_2").css('color', "grey" );
            $("#tde_3").css('color', "grey" );
            $("#tde_4").css('color', "grey");
            $("#tde_5").css('color', "grey" );
            break;
            case  2 :
            $("#tde_1").css('color', "orange" );
            $("#tde_2").css('color', "orange" );
            $("#tde_3").css('color', "grey" );
            $("#tde_4").css('color', "grey");
            $("#tde_5").css('color', "grey" );
            break;
            case 3 :
            $("#tde_1").css('color', "orange" );
            $("#tde_2").css('color', "orange" );
            $("#tde_3").css('color', "orange" );
            $("#tde_4").css('color', "grey");
            $("#tde_5").css('color', "grey" );
            break;
            case 4 :
            $("#tde_1").css('color', "orange" );
            $("#tde_2").css('color', "orange" );
            $("#tde_3").css('color', "orange" );
            $("#tde_4").css('color', "orange");
            $("#tde_5").css('color', "grey" );
            break;
            case 5 :
            $("#tde_1").css('color', "orange" );
            $("#tde_2").css('color', "orange" );
            $("#tde_3").css('color', "orange" );
            $("#tde_4").css('color', "orange");
            $("#tde_5").css('color', "orange" );
            break;
        }

    });

}





