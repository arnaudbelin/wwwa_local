<?php 

class Admin extends CI_Controller {

	public function index(){

		if($this->session->userdata('logged_in_admin') == 1){

			$data['dates'] = $this->tour_model->get_all_dates();

			$data['news'] = $this->infos_model->get_all_news();

			$data['messages'] = $this->admin_model->get_all_message();

			$data['emails_newsletter'] = $this->admin_model->get_all_email();

			$data['bookedtickets'] = $this->admin_model->get_all_booked_tickets();

			$data['main_view'] = "admin_view";

			$this->load->view('layouts/main', $data);

		}else{

			redirect('home');

		}

	}

	public function display_update_date($id_concert){

		$data['date_u'] = $this->admin_model->get_date($id_concert);

		$data['main_view'] = "date_update_view";

		$this->load->view('layouts/main', $data);

	}

	public function delete_date($id_concert){

		$this->admin_model->delete_date($id_concert);

		redirect('admin');

	}

	public function done_message($id_message){

		$result = $this->admin_model->done_message($id_message);

		if($result){

			redirect('admin');

		}

	}

	public function create_date(){

		if($this->input->post('city') == NULL){

			$data['main_view'] = "date_create_view";

			$this->load->view('layouts/main', $data);

		}else{

			$this->form_validation->set_rules('city', 'City', 'trim|htmlspecialchars|required');
			$this->form_validation->set_rules('country', 'Country', 'trim|htmlspecialchars|required');
			$this->form_validation->set_rules('concert_hall', 'Concert hall', 'trim|htmlspecialchars|required');
			$this->form_validation->set_rules('date', 'Date', 'trim|htmlspecialchars|required');

			if($this->form_validation->run()== FALSE){

				$data['main_view'] = "date_create_view";

				$this->load->view('layouts/main', $data);

			} else {

				$data = array(

					'city' => $this->input->post('city'),
					'country' => $this->input->post('country'),
					'concert_hall' => $this->input->post('concert_hall'),
					'date' => $this->input->post('date')

				);

				$data = $this->security->xss_clean($data);


				if($this->admin_model->create_date($data)){

					redirect("admin/index");

				}

			}

		}

	}

	public function update_date($id_concert){

		$this->form_validation->set_rules('city', 'City', 'trim|htmlspecialchars|required');
		$this->form_validation->set_rules('country', 'Country', 'trim|htmlspecialchars|required');
		$this->form_validation->set_rules('concert_hall', 'Concert hall', 'trim|htmlspecialchars|required');
		$this->form_validation->set_rules('date', 'Date', 'trim|htmlspecialchars|required');

		if($this->form_validation->run()== FALSE){

			$data['main_view'] = "date_update_view";

			$this->load->view('layouts/main', $data);

		} else {

			$data = array(

				'city' => $this->input->post('city'),
				'country' => $this->input->post('country'),
				'concert_hall' => $this->input->post('concert_hall'),
				'date' => $this->input->post('date')

			);

			$data = $this->security->xss_clean($data);

			if($this->admin_model->edit_date($id_concert, $data)){

				redirect("admin/index");

			}

		}

	}

	public function delete_news($id_news){

		$this->admin_model->delete_news($id_news);

		redirect('admin');

	}

	public function update_news($id_news){

		
		$this->form_validation->set_rules('title_news', 'Title', 'trim|htmlspecialchars|required');

		$this->form_validation->set_rules('body_news', 'Body', 'trim|htmlspecialchars|required');

		if($this->form_validation->run()== FALSE){

			$data['main_view'] = "news_update_view";

			$this->load->view('layouts/main', $data);

		} else {

			$data = array(

				'title_news' => $this->input->post('title_news'),
				'body_news' => $this->input->post('body_news')

			);

			$data = $this->security->xss_clean($data);

			if($this->admin_model->update_news($id_news, $data)){

				redirect("admin/index");
			}

		}

	}

	public function display_update_news($id_news){

		$data['news_u'] = $this->admin_model->get_news($id_news);

		$data['main_view'] = "news_update_view";

		$this->load->view('layouts/main', $data);

	}

	public function create_news(){

		if($this->input->post('title_news') == NULL){

			$data['main_view'] = "news_create_view";

			$this->load->view('layouts/main', $data);

		}else{

			$this->form_validation->set_rules('title_news', 'Title', 'trim|htmlspecialchars|required');

			$this->form_validation->set_rules('body_news', 'Body', 'trim|htmlspecialchars|required');

			if($this->form_validation->run()== FALSE){

				$data['main_view'] = "news_create_view";

				$this->load->view('layouts/main', $data);

			} else {

				$data = array(

					'title_news' => $this->input->post('title_news'),
					'body_news' => $this->input->post('body_news'),
					'date_news' => date('Y-m-d')

				);

				$data = $this->security->xss_clean($data);

				if($this->admin_model->create_news($data)){

					redirect("admin/index");

				}


			}


		}


	}

}

?>