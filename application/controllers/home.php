<?php 

class Home extends CI_Controller {

	public function index(){

		$data['date'] = $this->tour_model->get_comming_date();

		$data['main_view'] = "home_view";

		$this->load->view('layouts/main', $data);

	}

	public function news(){

		$data['news'] = $this->infos_model->get_all_news();

		$data['main_view'] = "news_view";

		$this->load->view('layouts/main', $data);

	}

	public function rating(){

		$ip = $this->input->ip_address();

		$select = $this->input->get('param1');

		$data = array (
			
			'rate' => $select,
			'ip_adress' => $ip
			
		);
		$data = $this->security->xss_clean($data);

		$result = $this->infos_model->rating_album($data);

		if($result == false){

			$ko = array (
				'status' => 'error'
			);

			echo json_encode($ko);

		}else{

			echo json_encode($result);

		}

	}

	public function newsletter(){

		$this->form_validation->set_rules('email_newsletter', 'Email', 'trim|htmlspecialchars|required|min_length[3]|valid_email');
		
		if($this->form_validation->run() == FALSE){

			$data['main_view'] = 'home_view';

			$this->load->view('layouts/main', $data);
		}else{

			$email = $this->input->post('email_newsletter');

			$email = $this->security->xss_clean($email);

			$result = $this->infos_model->newsletter($email);

			if($result){
				$this->session->set_flashdata('newsletter_success', 'your have suscribe to our newsletter');

				redirect('home');
			}
		}
	}

	public function album(){

		$data['average'] = $this->infos_model->average_rate();

		$data['main_view'] = "album_view";

		$this->load->view('layouts/main', $data);


	}

	public function get_average(){

		$avg = $this->infos_model->average_rate();

		echo json_encode($avg);

	}

	public function comment(){

		if($this->input->get('task') == 'write'){

			$disallowed = array('anal','anus','arse','ass','ballsack','balls','bastard','bitch','biatch','bloody','blowjob','blowjob','bollock','bollok','boner','boob','bugger','bum','butt','buttplug','clitoris','cock','coon','crap','cunt','damn','dick','dildo','dyke','fag','fagot','feck','fellate','fellatio','felching','fuck','fudgepacker','flange','jerk','jizz','hitler','knobend','labia','muff','nigger','nigga','penis','piss','poop','prick','pube','pussy','queer','scrotum','sex','scum','shit','sh1t','slut','smegma','spunk','twat','vagina','wank','whore');
			$content = word_censor($this->input->post('content'), $disallowed);	

			$author = word_censor($this->input->post('author'), $disallowed);	

			$data = array (

				'author' => $author,
				'content' => $content

			);
			
			if($this->input->post('author') == '' OR $this->input->post('content') == '' ){

				$ko = "ko";

				echo json_encode($ko);

			}else{
				$data = html_escape($data);

				$data = $this->security->xss_clean($data);

				$this->comment_model->post_comment($data);

			}

		}else{

			$comments = $this->comment_model->get_comment();

			echo json_encode($comments);

		}

	}

	public function contact(){

		$data['main_view'] = "contact_view";

		$this->load->view('layouts/main', $data);

	}

	public function contact_us(){

		if(!$this->session->userdata('logged_in')){

			$this->session->set_flashdata('no_access', 'Sorry, you must be logged ');

			redirect('home/contact');
		}else{

			$this->form_validation->set_rules('title', 'Title', 'trim|required|htmlspecialchars');
			$this->form_validation->set_rules('description', 'Description', 'trim|required|htmlspecialchars|min_length[2]');

			if($this->form_validation->run() == FALSE){

				$data['main_view'] = 'contact_view';

				$this->load->view('layouts/main', $data);
			}else{

				$data = array (
					
					'user_id' => $this->session->userdata('user_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description')
					
				);

				$data = $this->security->xss_clean($data);
				
				$result = $this->infos_model->contact($data);

				if($result){
					$this->session->set_flashdata('contact_success', 'your request has been successfully registered');
					redirect('home');
				}
			}
		}
	}


	public function tour_date(){

		$data['dates'] = $this->tour_model->get_all_dates();

		$data['main_view'] = "tour_date_view";

		$this->load->view('layouts/main', $data);

	}


	public function book($id_concert){


		if(!$this->session->userdata('logged_in')){

			$this->session->set_flashdata('no_access', 'Sorry, you must be register or login for booking');

			redirect('home/index');
		}else{

			$data['concert'] = $this->tour_model->book_concert($id_concert);

			$data['main_view'] = "book_view";

			$this->load->view('layouts/main', $data);

		}


	}

	public function book_ticket($id_concert){


		$this->load->library('Pdf');

		$book_number = html_escape($this->input->post('book_number'));

		$book_number = $this->security->xss_clean($book_number);

		$data = array (

			'concert_id' => $id_concert,
			'user_id' => $this->session->userdata('user_id'),
			'user_email' => $this->session->userdata('email'),
			'book_number' => $book_number
			
		);

		$result_query = $this->tour_model->booked($id_concert, $data);

		$data['concert'] = $this->tour_model->book_concert($id_concert);

		$data['booked'] = $this->tour_model->book_number($id_concert);


		if($result_query){

			$this->load->view('makepdf_view', $data);

		// 	$this->load->library('email');

		// 	$CI = get_instance();

		// 	$config = array();
		// 	$config['protocol'] = 'smtp';
		// 	$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		// 	$config['smtp_port'] = 465;
		// 	$config['smtp_user'] = 'arewealivehxc@gmail.com';
		// 	$config['smtp_pass'] = 'anarchyinhxc';
			



		// 	$CI->email->initialize($config);



		// 	$CI->email->send();

		// 	$this->email->from('arewealivehxc@gmail.com', 'Are we alive?');
		// 	$this->email->to('arewealivehxc@gmail.com');
			

		// 	$this->email->subject('Your tickets');
		// 	$this->email->message('Great !!');

		// 	$this->email->send();

		}else{

			$this->session->set_flashdata('problem_booked', 'Sorry, we have problem for booking tickets.<br> Please contact us for more informations');
		}

	}

}

?>




