<div class="container col-12 d-flex justify-content-around" id="contactback" >
	<div id="contact"  >
		<p class="bg-danger">
			<?php if($this->session->flashdata('no_access')): ?>
				<?php echo $this->session->flashdata('no_access')?>
				<?php endif; ?></p>
				<h2>Contact</h2>
				<?php if($this->session->userdata('logged_in') == false):?> 
					<p>You must be logged for use this contact form</p>  
				<?php endif; ?>
				<?php $attributes = array('id' => 'contact_form', 'class'=>'form') ?>
				<?php echo validation_errors("<p class = 'alert alert-danger'/p>"); ?>
				<?php echo form_open('home/contact_us', $attributes); ?>
				<div class="form-group">	
					<?php echo form_label('Title'); ?>
					<?php 
					$data = array(
						'class' => 'form-control',
						'name' => 'title'	
					);
					?>
					<?php echo form_input($data); ?>
				</div>
				<div class="form-group">	
					<?php echo form_label('Description'); ?>
					<?php 
					$data = array(
						'class' => 'form-control',
						'name' => 'description'
					);
					?>
					<?php echo form_textarea($data); ?>
				</div>
				<div class="form-group">
					<?php 
					$data = array(
						'class' => 'btn bouton',
						'name' => 'submit', 
						'value'=> 'SUBMIT'
					);
					?>
					<?php echo form_submit($data); ?>
				</div>
	</div>
</div>
		<?php echo form_close(); ?>




