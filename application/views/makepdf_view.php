<?php  

$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Tickets');
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(20);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Are we alive?');
$pdf->SetDisplayMode('real', 'default');

$pdf->AddPage();

$booked_id = $booked->booked_id;
$last_name =  $this->session->userdata('last_name');
$first_name = $this->session->userdata('first_name');
$concert_city = $concert->city; 
$concert_hall =$concert->concert_hall;
$concert_date = $concert->date;
$number_ticket = $booked->book_number;
$html = '
<img src="'.base_url().'assets/image/qrcode.png" alt="test alt attribute" width="100" height="100" border="0" />
<h1>TICKET :</h1>
<div>
  	<label><b>Booking n°</b> '.$booked_id.'</label>
</div>
<div>
  	<label><b>Name</b> : '.$first_name.', '.$last_name.'</label>
</div>
<div>
  	<label><b>City :</b> '.$concert_city.'</label>
</div>
<div>
  	<label><b>Hall :</b> '.$concert_hall.'</label>
</div>
<div>
  	<label><b>Date :</b> '.$concert_date.'</label>
</div>
<div>
  	<label><b>Number of tickets :</b> '.$number_ticket.'</label>
</div>
';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
ob_clean();
$pdf->Output('My-tickets.pdf', 'I');

?>