
<div class="container  " id="admin">
	<div id="sscontainer_admin" >
		<h4>Tour date</h4>
		<a href="<?php echo base_url(); ?>admin/create_date " class="btn bouton">Add New Date</a>
		<div class="table-responsive">
			<table class ="table">
				<thead>
					<tr>
						<th>
							City / Country
						</th>

						<th>
							Hall
						</th>
						<th>
							Date
						</th>
						<th>
							Number of booking
						</th>
						<th>
							Update
						</th>
						<th>
							Delete
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($dates as $date): ?>
						<tr>
							<?php echo "<td>".$date->city.", ".$date->country."</td>" ;?> 
							<?php echo "<td>".$date->concert_hall."</td>" ;?>
							<?php echo "<td>".$date->date."</td>" ;?>
							<?php echo "<td>".$date->book."</td>" ;?>
							<td> <a class="btn btn-success"href="<?php echo base_url(); ?>admin/display_update_date/<?php echo $date->id ?>"><span>U</span></a></td>
							<td> <a class="btn btn-danger"href="<?php echo base_url(); ?>admin/delete_date/<?php echo $date->id ?>"><span>X</span></a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<h4>Booked tickets</h4>
		<div class="table-responsive">
			<table class ="table">
				<thead>
					<tr>
						<th>
							Email 
						</th>

						<th>
							Name
						</th>
						<th>
							Concert
						</th>
						<th>
							Date
						</th>
						<th>
							Number tickets
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($bookedtickets as $bookedticket): ?>
						<tr>
							<?php echo "<td>".$bookedticket->email."</td>" ;?> 
							<?php echo "<td>".$bookedticket->first_name."</td>" ;?>
							<?php echo "<td>".$bookedticket->concert_hall."</td>" ;?>
							<?php echo "<td>".$bookedticket->date."</td>" ;?>
							<?php echo "<td>".$bookedticket->book_number."</td>" ;?>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<h4>News</h4>
		<a href="<?php echo base_url(); ?>admin/create_news " class="btn bouton">Add News</a>
		<div class="table-responsive">
			<table class ="table">
				<thead>
					<tr>
						<th>
							Tittle
						</th>
						<th>
							Date
						</th>
						<th>
							Body
						</th>
						<th>
							Update
						</th>
						<th>
							Delete
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($news as $new): ?>
						<tr>
							<?php echo "<td>".$new->title_news."</td>" ;?> 
							<?php echo "<td>".$new->date_news."</td>" ;?>
							<?php echo "<td>".$new->body_news."</td>" ;?>

							<td> <a class="btn btn-success"href="<?php echo base_url(); ?>admin/display_update_news/<?php echo $new->id ?>"><span>U</span></a></td>

							<td> <a class="btn btn-danger"href="<?php echo base_url(); ?>admin/delete_news/<?php echo $new->id ?>"><span>X</span></a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<h4>Message from Contact Form</h4>
		<div class="table-responsive">
			<table class ="table">
				<thead>
					<tr>
						<th class="email_contact">
							Email 
						</th>

						<th>
							tittle
						</th>
						<th>
							Body
						</th>
						<th>
							Reply
						</th>
						<th>
							Done
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($messages as $message): ?>
						<tr>
							<?php echo "<td class='email_contact'>".$message->email."</td>" ;?> 
							<?php echo "<td>".$message->title."</td>" ;?>
							<?php echo "<td>".$message->description."</td>" ;?>
							<td> <a class="btn bouton"href="mailto:<?php echo $message->email?>?subject=Your%20reply%20from%20Are%20we Alive&body=Your%20message%20from%20our%20contact%20form%20: <?php echo $message->description ?>"><span>R</span></a></td>
							<td> <a class="btn btn-success"href="<?php echo base_url(); ?>admin/done_message/<?php echo $message->id_message ?>"><span>D</span></a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div>
			<h4>Send Newsletter</h4>
			<a href="mailto:<?php foreach ($emails_newsletter as $email_newsletter):?> <?php echo $email_newsletter->email_newsletter. ','?> <?php endforeach; ?> " class="btn bouton">NEWSLETTER</a>
		</div>
		<br>
	</div>
</div>