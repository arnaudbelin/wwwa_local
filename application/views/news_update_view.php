<div id="update_news" class="container">
  <?php $attributes = array('class'=>'form') ?>
  <?php echo validation_errors("<p class = 'alert alert-danger'/p>"); ?>
  <?php echo form_open("admin/update_news/".$news_u->id."", $attributes); ?>
  <div class="form-group">
    <?php echo form_label('Title'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'title_news',
      'value' => $news_u->title_news
    );
    ?>
    <?php echo form_input($data); ?>
  </div>
  <div class="form-group">
    <?php echo form_label('Body'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'body_news',
      'value' => $news_u->body_news
    );
    ?>
    <?php echo form_textarea($data); ?>
  </div>
  <div class="form-group">
    <?php 
    $data = array(
      'class' => 'btn bouton',
      'value' => 'Update'
    );
    ?>
    <?php echo form_submit($data); ?>
  </div>
  <?php echo form_close(); ?>
</div>