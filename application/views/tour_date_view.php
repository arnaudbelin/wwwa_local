<div  id="tourdate" class="container col-12 d-flex justify-content-around">
    <div id="blur" class="col-12 d-flex align-items-center justify-content-center">
        <div class="row">
            <div class="col-12 justify-content-center">    
                <br>
            </div>
            <?php foreach ($dates as $date): ?>
              <div id="date" class="col-12 border-top d-flex justify-content-between">
                  <div id="list_date"  class="text-light">
                    <ul>
                        <?php echo "<li>".$date->city.", ".$date->country."</li>" ;?> 
                        <?php echo "<li>".$date->concert_hall."</li>" ;?>
                        <?php echo "<li>".$date->date."</li>" ;?>
                    </ul>
                </div>
                <div class="d-flex align-items-center" >
                    <a href="<?php base_url(); ?>book/<?php echo $date->id ?>">BOOK</a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
</div>