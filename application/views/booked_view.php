
<div class="container col-12 d-flex justify-content-around" id="" >
<div id=""  >


  <div class="row">
<div id=""  class="text-dark col-8 m-5">
  

  <form >
    <div class="form-group">

    <label for="book">Booking n°</label>
    
    <input type="text" class="form-control text-capitalize" name="book" value="<?php echo $booked->booked_id; ?>" disabled>
    
  </div>
  <div class="form-group">

    <label for="name">Name</label>
    
    <input type="text" class="form-control text-capitalize" name="name" value="<?php echo $this->session->userdata('first_name'); ?>, <?php echo $this->session->userdata('last_name');  ?>" disabled>
    
  </div>
  <div class="form-group">

    <label for="city">City :</label>
    
    <input type="text" class="form-control text-capitalize" name="city" value="<?php echo $concert->city; ?>" disabled>
    
  </div>
  <div class="form-group">

    <label for="country">Country :</label>
    
    <input type="text" class="form-control text-uppercase" name="country" value="<?php echo $concert->country; ?>" disabled>

  </div>
 <div class="form-group">
    <label for="concert_hall">Concert Hall :</label>
    <input type="text" class="form-control text-capitalize" name="concert_hall" value="<?php echo $concert->concert_hall; ?>" disabled>

  </div>

  <div class="form-group">

    <label for="date">Date :</label>
    
    <input type="text" class="form-control" name="date" value="<?php echo $concert->date; ?>" disabled>

  </div>
  <div class="form-group">

    <label for="tickets">Number of tickets :</label>
    
    <input type="text" class="form-control" name="tickets" value="<?php echo $booked->book_number; ?>" disabled>

  </div>
 
  
  <input id="print" class="btn bouton" type="button" value="Print" onClick="window.print()">
</form>

</div>
                    
                </div>
             </div>
                    
                </div>