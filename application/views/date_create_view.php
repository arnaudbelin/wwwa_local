<div id="update_date" class="container">
  <?php $attributes = array('class'=>'form') ?>
  <?php echo validation_errors("<p class = 'alert alert-danger'/p>"); ?>
  <?php echo form_open("admin/create_date", $attributes); ?>
  <div class="form-group">
    <?php echo form_label('City'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'city'
    );
    ?>
    <?php echo form_input($data); ?>
  </div>
  <div class="form-group">
    <?php echo form_label('Country'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'country'
    );
    ?>
    <?php echo form_input($data); ?>
  </div>
  <div class="form-group">
    <?php echo form_label('Concert hall'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'concert_hall'
    );
    ?>
    <?php echo form_input($data); ?>
  </div>
  <div class="form-group"> 
    <?php echo form_label('Date : YYYY-MM-DD'); ?>
    <?php 
    $data = array(
      'class' => 'form-control',
      'name' => 'date'
    );
    ?>
    <?php echo form_input($data); ?>
  </div>
  <div class="form-group">
    <?php 
    $data = array(
      'class' => 'btn bouton',
      'value' => 'CREATE'
    );
    ?>
    <?php echo form_submit($data); ?>
  </div>
  <?php echo form_close(); ?>
</div>