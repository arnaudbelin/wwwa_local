<?php 

class Infos_model extends CI_Model{

	public function contact($data){

		$insert_data = $this->db->insert('contact', $data);

		return $insert_data;

	}

	public function newsletter($email){

		$data = array (
			
			'email_newsletter' => $email	
		);

		$this->db->where('email_newsletter', $email);

		$create = $this->db->get('newsletter');

		$already_create = $create->row();

		if(isset($already_create)){

			$this->session->set_flashdata('already_create', 'Sorry, this email adress is already use.');

			redirect('home/index');

		}else{

			$insert_data = $this->db->insert('newsletter', $data);

			return $insert_data;
		}

	}

	public function get_all_news(){

		$query = $this->db->get('news');

		return $query->result();

	}

	public function rating_album($data){

		$this->db->where('ip_adress', $data['ip_adress']);

		$create = $this->db->get('rating_album');

		$already_create = $create->row();

		if(isset($already_create)){

			return false;

		}else{

			$insert = $this->db->insert('rating_album', $data);

			return $insert;
		}	

	}

	public function average_rate(){

		$this->db->select_avg('rate');
		
		$query = $this->db->get('rating_album');


		return $query->row();
		

	}




}

?>